## Xamarin and CI
https://developer.xamarin.com/guides/cross-platform/ci/intro_to_ci/


## Xamarin and Jenkins incompatibility
https://developer.xamarin.com/guides/cross-platform/troubleshooting/questions/xamarin-jenkins/


## Oktopeus Restboy Jenkins server
https://cpaschoulas.no-ip.org:10443


## Install Mono Tutorial
http://getaclue.me/2014/10/11/Installing-Mono-on-CentOS-7/
http://www.mono-project.com/docs/getting-started/install/linux/


## Install & Configure Jenkins service
https://wiki.jenkins-ci.org/display/JENKINS/Installing+Jenkins+on+Red+Hat+distributions
https://wiki.jenkins-ci.org/display/JENKINS/Starting+and+Accessing+Jenkins
