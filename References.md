
Eto - Cross platform GUI framework for desktop and mobile applications in .NET
https://github.com/picoe/Eto


Stateless 3.0 - A State Machine library for .NET Core
http://www.hanselman.com/blog/Stateless30AStateMachineLibraryForNETCore.aspx


.NET Standard Library Support for Xamarin
https://blog.xamarin.com/net-standard-library-support-for-xamarin/


.NET Standard Library
https://docs.microsoft.com/en-us/dotnet/articles/standard/library


Realm replacement for SQLite
https://github.com/realm/realm-dotnet


SonarQube
http://www.sonarqube.org/
http://vssonarextension.blogspot.gr/


Restboy whitepaper
https://oktopeus.slack.com/files/hampos/F317B3ZL5/restboy-whitepaper-v0.0.1.pdf


Jenkins
http://blog.bekijkhet.com/2013/01/create-mono-c-buildserver-using-jenkins.html
https://github.com/74th/jenkins-dotnet
https://github.com/kpaulus/jenkins-mono
