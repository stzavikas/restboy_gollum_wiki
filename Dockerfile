FROM robinthrift/raspbian-gollum 

VOLUME /home/pi/utilities/dockerfiles/restboy_doc 

WORKDIR /home/pi/utilities/dockerfiles/restboy_doc 

CMD ["gollum", "--port", "80", "--config", "config.rb", "--css"]

EXPOSE 80
